const { pathsToModuleNameMapper } = require('ts-jest/utils');
const { compilerOptions } = require('./tsconfig.json');

module.exports = {
  preset: 'jest-preset-angular',
  globals: {
    'ts-jest': {
      tsConfig: '<rootDir>/tsconfig.spec.json',
      stringifyContentPathRegex: '\\.html$',
      astTransformers: ['jest-preset-angular/InlineHtmlStripStylesTransformer']
    }
  },
  setupFilesAfterEnv: ['<rootDir>/src/jest-setup.ts'],
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths),
  modulePaths: ['<rootDir>'],
  collectCoverageFrom: ['**/*.ts', '!**/node_modules/**'],
  coverageReporters: ['html', 'text'],
  coveragePathIgnorePatterns: [
    '<rootDir>/.vscode/',
    '<rootDir>/releases/',
    '<rootDir>/coverage/',
    '<rootDir>/node_modules/'
  ]
};
