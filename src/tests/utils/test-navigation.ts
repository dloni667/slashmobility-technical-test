import { Location } from '@angular/common';
import { ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { Router } from '@angular/router';

/**
 * testNavigation<T>()
 *
 * @description Helper for testing routes
 * @param location: Location
 * @param router: Router
 * @param fixture: ComponentFixture<T>
 * @param navigate: string[]
 * @param routeExpected: string
 * @return void
 */
export function testNavigation<T>(
  location: Location,
  router: Router,
  fixture: ComponentFixture<T>,
  navigate: string[],
  routeExpected: string
): void {
  fakeAsync(() => {
    fixture.ngZone.run(() => {
      router.initialNavigation();
      router.navigate(navigate);
      tick();
      expect(location.path()).toBe(routeExpected);
    });
  });
}
