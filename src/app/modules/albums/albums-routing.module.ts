import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListAlbumsComponent } from './pages/list-albums/list-albums.component';

export const routes: Routes = [
  {
    path: '',
    component: ListAlbumsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumsRoutingModule { }
