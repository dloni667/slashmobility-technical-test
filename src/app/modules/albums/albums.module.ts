import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';

import { SharedModule } from '@app/shared/shared.module';
import { AlbumsRoutingModule } from './albums-routing.module';
import { AlbumCardComponent } from './component/album-card/album-card.component';
import { ListAlbumsComponent } from './pages/list-albums/list-albums.component';

@NgModule({
  declarations: [
    ListAlbumsComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    AlbumsRoutingModule,
    MatCardModule,
    MatToolbarModule,
    MatInputModule,
    MatIconModule,
    FlexLayoutModule,
    LayoutModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatBadgeModule
  ]
})
export class AlbumsModule { }
