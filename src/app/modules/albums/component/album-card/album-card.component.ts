import { Component, EventEmitter, Input, Output } from '@angular/core';

import { MediaType } from '@app/models/media-type';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss']
})
export class AlbumCardComponent {
  @Input() public album: MediaType;
  @Output() public updateAlbumFavorite = new EventEmitter<MediaType>();

  constructor() { }

  changeFavorite() {
    this.album.favorite = !this.album.favorite;
    this.updateAlbumFavorite.emit(this.album);
  }
}
