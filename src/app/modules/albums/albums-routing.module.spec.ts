import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule, Location } from '@angular/common';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { FavoritesBadgeComponent } from '@app/components/favorites-badge/favorites-badge.component';
import { IonSearchbarComponent } from '@app/components/ion-searchbar/ion-searchbar.component';
import { MenuItemComponent } from '@app/components/menu-item/menu-item.component';
import { MainNavComponent } from '@app/layout/main-nav/main-nav.component';
import { testNavigation } from '@tests/utils/test-navigation';

import { routes } from './albums-routing.module';
import { AlbumCardComponent } from './component/album-card/album-card.component';
import { ListAlbumsComponent } from './pages/list-albums/list-albums.component';

@Component({
  template: `
    <router-outlet></router-outlet>
  `
})
class AppComponent { }

describe('Router: Album', () => {
  let location: Location;
  let router: Router;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListAlbumsComponent,
        MenuItemComponent,
        MainNavComponent,
        IonSearchbarComponent,
        FavoritesBadgeComponent,
        AlbumCardComponent,
        AppComponent,
      ],
      imports: [
        CommonModule,
        FormsModule,
        MatCardModule,
        MatToolbarModule,
        MatInputModule,
        MatIconModule,
        FlexLayoutModule,
        LayoutModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatBadgeModule,
        RouterTestingModule.withRoutes(routes),
      ],
    });

    router = TestBed.get(Router);
    location = TestBed.get(Location);

    fixture = TestBed.createComponent(AppComponent);
  });

  describe('List', () => {
    it('should user access', () => {
      testNavigation(location, router, fixture, [''], '/');
    });
  });

  describe('With prefix', () => {
    const routePrefix = 'search';
    beforeEach(() => {
      router.resetConfig([
        {
          path: routePrefix,
          component: AppComponent,
          children: [
            ...routes
          ]
        },
      ]);
    });

    describe('List', () => {
      it('should user access', () => {
        testNavigation(location, router, fixture, [`/${routePrefix}`], `/${routePrefix}`);
      });
    });
  });
});
