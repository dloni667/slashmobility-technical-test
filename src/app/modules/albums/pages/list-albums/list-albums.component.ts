import { Component } from '@angular/core';

import { MediaType } from '@app/models/media-type';
import { MediaTypePaginate } from '@app/models/media-type-paginate';
import { SearchService } from '@app/services/search.service';

@Component({
  selector: 'app-list-albums',
  templateUrl: './list-albums.component.html',
  styleUrls: ['./list-albums.component.scss']
})
export class ListAlbumsComponent {
  public albums: MediaTypePaginate = {
    resultCount: 0,
    results: []
  };

  constructor(private searchService: SearchService) { }

  public searchAlbum(search: string): void {
    this.searchService.search(search, 'album')
      .subscribe((albums: MediaTypePaginate) => {
        albums.results.map((album: MediaType) => {
          album.favorite = false;
        });

        this.albums = albums;
      });
  }

  public updateAlbumFavorite(album: MediaType): void {
    const filterAlbums = this.albums.results.filter(albumResult => album.collectionId === albumResult.collectionId);
    filterAlbums.map(albumResult => albumResult.favorite = album.favorite);
  }

  public countFavorites(): number {
    return this.albums.results.filter((album: MediaType) => album.favorite).length;
  }
}
