import { Component, EventEmitter, Input, Output } from '@angular/core';

import { MediaType } from '@app/models/media-type';

@Component({
  selector: 'app-song-card',
  templateUrl: './song-card.component.html',
  styleUrls: ['./song-card.component.scss']
})
export class SongCardComponent {
  @Input() public song: MediaType;
  @Output() public updateSongFavorite = new EventEmitter<MediaType>();

  constructor() { }

  changeFavorite() {
    this.song.favorite = !this.song.favorite;
    this.updateSongFavorite.emit(this.song);
  }
}
