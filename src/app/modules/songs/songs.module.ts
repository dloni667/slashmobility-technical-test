import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';

import { SharedModule } from '@app/shared/shared.module';
import { SongCardComponent } from './component/song-card/song-card.component';
import { ListSongsComponent } from './pages/list-songs/list-songs.component';
import { SongsRoutingModule } from './songs-routing.module';

@NgModule({
  declarations: [
    ListSongsComponent,
    SongCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    SongsRoutingModule,
    MatCardModule,
    MatToolbarModule,
    MatInputModule,
    MatIconModule,
    FlexLayoutModule,
    LayoutModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatBadgeModule
  ]
})
export class SongsModule { }
