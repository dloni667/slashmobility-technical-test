import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListSongsComponent } from './pages/list-songs/list-songs.component';

export const routes: Routes = [
  {
    path: '',
    component: ListSongsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SongsRoutingModule { }
