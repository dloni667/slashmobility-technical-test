import { Component } from '@angular/core';

import { MediaType } from '@app/models/media-type';
import { MediaTypePaginate } from '@app/models/media-type-paginate';
import { SearchService } from '@app/services/search.service';

@Component({
  selector: 'app-list-songs',
  templateUrl: './list-songs.component.html',
  styleUrls: ['./list-songs.component.scss']
})
export class ListSongsComponent {
  public songs: MediaTypePaginate = {
    resultCount: 0,
    results: []
  };

  constructor(private searchService: SearchService) { }

  public searchSong(search: string): void {
    this.searchService.search(search, 'song')
      .subscribe((songs: MediaTypePaginate) => {
        songs.results.map((song: MediaType) => {
          song.favorite = false;
        });

        this.songs = songs;
      });
  }

  public updateSongFavorite(song: MediaType): void {
    const filterSongs = this.songs.results.filter(songResult => song.trackId === songResult.trackId);
    filterSongs.map(songResult => songResult.favorite = song.favorite);
  }

  public countFavorites(): number {
    return this.songs.results.filter((song: MediaType) => song.favorite).length;
  }
}
