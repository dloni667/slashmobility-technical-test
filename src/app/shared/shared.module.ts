import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';

import { FavoritesBadgeComponent } from '@app/components/favorites-badge/favorites-badge.component';
import { IonSearchbarComponent } from '@app/components/ion-searchbar/ion-searchbar.component';
import { MenuItemComponent } from '@app/components/menu-item/menu-item.component';
import { MainNavComponent } from '@app/layout/main-nav/main-nav.component';

@NgModule({
  declarations: [
    MenuItemComponent,
    MainNavComponent,
    IonSearchbarComponent,
    FavoritesBadgeComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    MatToolbarModule,
    MatInputModule,
    MatIconModule,
    FlexLayoutModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatBadgeModule
  ],
  exports: [
    MenuItemComponent,
    MainNavComponent,
    IonSearchbarComponent,
    FavoritesBadgeComponent,
  ],
})
export class SharedModule { }
