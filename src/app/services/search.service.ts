import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilsService } from '@app/services/utils.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  static URL = 'https://itunes.apple.com/search';

  constructor(private http: HttpClient) { }

  /**
   * page()
   *
   * @memberOf SearchService
   * @param queryObj: object
   * @return Observable<any>
   */
  search(song: string, type: string): Observable<any> {
    const query = UtilsService.serializeUrl({
      term: song,
      entity: type
    });

    return this.http.get<any>(`${SearchService.URL}?${query}`).pipe(catchError(this.handleError('searchSong', [])));
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  protected handleError<T>(operation = 'operation', result?: T): any {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed`, error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
