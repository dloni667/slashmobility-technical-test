import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { MediaTypePaginate } from '@app/models/media-type-paginate';
import { UtilsService } from '@app/services/utils.service';
import { songsFixture } from '@app/tests/fixtures/media-type';
import { SearchService } from './search.service';

describe('SearchService', () => {
  let httpMock: HttpTestingController;
  let service: SearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    // inject the service
    service = TestBed.get(SearchService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    service = TestBed.get(SearchService);
    expect(service).toBeTruthy();
  });

  it('should be search', () => {
    const { list: listSongs } = songsFixture;
    const searchSong = 'song';

    const request = service.search(searchSong, 'song');
    const query = UtilsService.serializeUrl({
      term: searchSong,
      entity: 'song'
    });

    request.subscribe((data: MediaTypePaginate) => {
      expect(data).toEqual(listSongs);
    });

    const req = httpMock.expectOne(`${SearchService.URL}?${query}`);
    expect(req.request.method).toBe('GET');

    req.flush(listSongs);
  });
});
