import { MediaType } from './media-type';

export interface MediaTypePaginate {
  resultCount: number;
  results: MediaType[];
  [key: string]: any;
}
