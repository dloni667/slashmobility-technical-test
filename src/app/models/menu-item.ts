export interface MenuItem {
  name: string;
  link: string[];
  [key: string]: any;
}
