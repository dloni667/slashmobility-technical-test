import { MenuItem } from '../../models/menu-item';

export const menu: MenuItem[] = [
  {
    name: 'Canciones',
    link: ['/songs']
  },
  {
    name: 'Álbumes',
    link: ['/albums']
  }
];
