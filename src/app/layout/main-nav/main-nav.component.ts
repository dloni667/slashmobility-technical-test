import { Component, Input } from '@angular/core';
import { MenuItem } from '@app/models/menu-item';
import { menu } from './menu';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent {
  @Input() public title: string;
  public menu: MenuItem[];

  constructor() {
    this.menu = menu;
  }
}
