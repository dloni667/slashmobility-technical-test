import { LayoutModule } from '@angular/cdk/layout';
import { APP_BASE_HREF } from '@angular/common';
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { MenuItemComponent } from '@app/components/menu-item/menu-item.component';
import { MainNavComponent } from './main-nav.component';

@Component({
  selector: 'app-test',
  template: `
  <app-main-nav title="Slash Mobility">
    <ng-container main-header>
    Title of main header
    </ng-container>
    <ng-container header-right>
      Title of header right
    </ng-container>
    <ng-container content>
      My special content
    </ng-container>
  </app-main-nav>
  `
})
class MainNavContentComponent { }

describe('MainNavComponent', () => {
  let component: MainNavComponent;
  let componentDE: DebugElement;
  let fixture: ComponentFixture<MainNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MainNavComponent,
        MenuItemComponent,
      ],
      imports: [
        NoopAnimationsModule,
        LayoutModule,
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatSidenavModule,
        MatToolbarModule,
        RouterModule.forRoot([])
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNavComponent);
    component = fixture.componentInstance;
    componentDE = fixture.debugElement;

    component.title = 'Welcome to app!';
    fixture.detectChanges();
  });

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));

  it('should render title in a h1 tag', async(() => {
    const titleContent = componentDE.query(By.css('[data-testID=title]'));
    expect(titleContent).toBeTruthy();
    expect(titleContent.nativeElement.textContent).toContain('Welcome to app!');
  }));
});

describe('MainNavContentComponent Content Projection', () => {
  let component: MainNavContentComponent;
  let fixture: ComponentFixture<MainNavContentComponent>;
  let componentDE: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MainNavContentComponent,
        MainNavComponent,
        MenuItemComponent,
      ],
      imports: [
        NoopAnimationsModule,
        LayoutModule,
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatSidenavModule,
        MatToolbarModule,
        RouterModule.forRoot([])
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNavContentComponent);
    component = fixture.componentInstance;
    componentDE = fixture.debugElement;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show app-main-nav', () => {
    const appMainNav = componentDE.query(By.css('app-main-nav'));
    expect(appMainNav).toBeTruthy();
  });

  it('should show main header content project', () => {
    const mainHeader = componentDE.query(By.css('[data-testID=main-header]'));
    expect(mainHeader).toBeTruthy();
    expect(mainHeader.nativeElement.textContent).toContain('Title of main header');
  });

  it('should show header right content project', () => {
    const headerRight = componentDE.query(By.css('[data-testID=header-right]'));
    expect(headerRight).toBeTruthy();
    expect(headerRight.nativeElement.textContent).toContain('Title of header right');
  });

  it('should show content project', () => {
    const content = componentDE.query(By.css('[data-testID=content]'));
    expect(content).toBeTruthy();
    expect(content.nativeElement.textContent).toContain('My special content');
  });
});
