import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  {
    path: 'songs',
    loadChildren: () =>
      import('./modules/songs/songs.module').then(
        m => m.SongsModule
      )
  },
  {
    path: 'albums',
    loadChildren: () =>
      import('./modules/albums/albums.module').then(
        m => m.AlbumsModule
      )
  },
  {
    path: '',
    redirectTo: 'songs',
    pathMatch: 'full'
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: 'not-found'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
