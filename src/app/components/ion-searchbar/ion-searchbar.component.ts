import { Component, EventEmitter, Output, OnDestroy, OnInit } from '@angular/core';

import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-ion-searchbar',
  templateUrl: './ion-searchbar.component.html',
  styleUrls: ['./ion-searchbar.component.scss']
})
export class IonSearchbarComponent implements OnInit, OnDestroy {
  @Output() public valueChanged = new EventEmitter<string>();
  public valueChange: Subject<string> = new Subject<string>();

  public value: string;
  private subscription: Subscription;

  constructor() { }

  changed(text: string) {
    this.valueChange.next(text);
  }

  ngOnInit() {
    this.subscription = this.valueChange
      .pipe(debounceTime(700), distinctUntilChanged())
      .subscribe((value: string) => {
        this.valueChanged.emit(value);
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
