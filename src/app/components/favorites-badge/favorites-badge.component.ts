import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-favorites-badge',
  templateUrl: './favorites-badge.component.html',
  styleUrls: ['./favorites-badge.component.scss']
})
export class FavoritesBadgeComponent {
  @Input() public favorites: number;

  constructor() { }
}
