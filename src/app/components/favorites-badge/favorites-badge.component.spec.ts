import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoritesBadgeComponent } from './favorites-badge.component';

describe.skip('FavoritesBadgeComponent', () => {
  let component: FavoritesBadgeComponent;
  let fixture: ComponentFixture<FavoritesBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FavoritesBadgeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoritesBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
